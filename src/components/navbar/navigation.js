import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import './navigation.css';
const Navigation = () => {
    return(
        <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
        <Navbar.Collapse id="basic-navbar-nav">
            <ul className="navigation">
                <li><Link to="/">Home</Link></li>
                <li><Link to="/about">About</Link></li>
                <li><Link to="/services">Services</Link></li>
            </ul>         
            
            
        </Navbar.Collapse>
        </Navbar>
    );
}
export default Navigation;