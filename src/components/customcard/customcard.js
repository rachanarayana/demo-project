import React from 'react';
import Card from 'react-bootstrap/Card';
const CustomCard = (props) => {
    return(
        <Card>
        <Card.Body>
            <Card.Title>{props.idText} - {props.title}</Card.Title>
            <Card.Text>{props.desc}</Card.Text>
        </Card.Body>
        </Card>
    );
}
export default CustomCard;