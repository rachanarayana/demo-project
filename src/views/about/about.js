import React, { Component, Fragment } from 'react';
import Loader from './../../components/loader/loader';
import AppConstants from './../../app-constants';
import Customcard from './../../components/customcard/customcard';
import { Container, Row, Col, Card } from 'react-bootstrap';
import axios  from 'axios';
class About extends Component {
    state = {
        jsonData: []
    }
    componentDidMount() {
        this.getData();
    }
    getData = () => {
        axios.get(AppConstants.BASE_URL + 'posts').then(response => {
            this.setState({ jsonData: response.data });
        });
    }
    render() {
        let loopData;
        if (this.state.jsonData.length === 0) {
            loopData = <Loader />;
        } else {
            loopData = this.state.jsonData.map((res) => {
                return (
                    <Col md={3}>
                        <Customcard idText={res.id} title={res.title} desc={res.body} />
                    </Col>)
            })
        }
        return (
            <Container>
                <Row>
                    {loopData}
                </Row>
            </Container>

        );
    }
}

export default About;
