import React, { Component, Fragment } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
class Home extends Component {
  state = {
    title: "",
    body: ""
  }
  captureData = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  submitedData = (e) => {
    e.preventDefault();
    console.log(this.state);
    this.setState({
      title: "",
      body: ""
    });
    // const { title, body } = this.state
    // axios.post(AppConstants.BASE_URL + 'posts/', { title, body }).then((res) => {
    //   console.log(res);
    // })
  }
  render() {
    return (
      <Form onSubmit={this.submitedData}>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>title</Form.Label>
                    <Form.Control type="text" placeholder="Enter title" name="title" value={this.state.title} onChange={this.captureData} />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" name="body" placeholder="Enter body" value={this.state.body} onChange={this.captureData} />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
    );
  }
}

export default Home;
