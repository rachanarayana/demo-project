import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Navigation from './components/navbar/navigation';
import Home from './views/home/home';
import About from './views/about/about';
import Services from './views/services/services';
function App() {
  return (
    <Router>
        <div>
          <Navigation />
          <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/about" exact component={About} />
                <Route path="/services" exact component={Services} />
            </Switch>
          </div>
        </Router>
  );
}

export default App;
